namespace LiveCharts.Definitions.Charts
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWindowAxisView : IAxisView
    {
        /// <summary>   Sets selected window. </summary>
        ///
        /// <param name="window">   The window. </param>

        void SetSelectedWindow(IAxisWindow window);
    }
}