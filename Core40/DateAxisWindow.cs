﻿//The MIT License(MIT)

//Copyright(c) 2016 Alberto Rodriguez & LiveCharts Contributors

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;

namespace LiveCharts
{
    /// <summary>   Form for viewing the date axis. </summary>
    ///

    public abstract class DateAxisWindow : AxisWindow
    {
        /// <summary>   Gets or sets the date axis core. </summary>
        ///
        /// <value> The date axis core. </value>

        public DateAxisCore DateAxisCore { get; set; }

        /// <summary>   Determines whether a dateTime is a header. </summary>
        ///
        /// <param name="x">    The x coordinate. </param>
        ///
        /// <returns>   <c>true</c> if header; otherwise <c>false</c> </returns>

        public override bool IsHeader(double x)
        {
            var date = DateAxisCore.GetdateTime(x);
            return IsHeader(date);
        }

        /// <summary>   Gets or sets a function to determine whether a dateTime is a separator. </summary>
        ///
        /// <param name="x">    The x coordinate. </param>
        ///
        /// <returns>   <c>true</c> if separator; otherwise <c>false</c> </returns>

        public override bool IsSeparator(double x)
        {
            var date = DateAxisCore.GetdateTime(x);
            return IsSeparator(date);
        }

        /// <summary>   Gets or sets a function to format the label for the axis. </summary>
        ///
        /// <param name="x">    The x coordinate. </param>
        ///
        /// <returns>   The formatted axis label. </returns>

        public override string FormatAxisLabel(double x)
        {
            var date = DateAxisCore.GetdateTime(x);
            return FormatAxisLabel(date);
        }

        /// <summary>   Query if 'x' is second. </summary>
        ///
        /// <param name="x">    The x coordinate Date/Time. </param>
        ///
        /// <returns>   <c>true</c> if second; otherwise <c>false</c> </returns>

        protected bool IsSecond(DateTime x)
        {
            return x.Millisecond == 0;
        }

        /// <summary>   Query if 'x' is minute. </summary>
        ///
        /// <param name="x">    The x coordinate Date/Time. </param>
        ///
        /// <returns>   <c>true</c> if minute; otherwise <c>false</c> </returns>

        protected bool IsMinute(DateTime x)
        {
            return x.Second == 0 && IsSecond(x);
        }

        /// <summary>   Query if 'x' is hour. </summary>
        ///
        /// <param name="x">    The x coordinate Date/Time. </param>
        ///
        /// <returns>   <c>true</c> if hour; otherwise <c>false</c> </returns>

        protected bool IsHour(DateTime x)
        {
            return x.Minute == 0 && IsMinute(x);
        }

        /// <summary>   Query if 'x' is day. </summary>
        ///
        /// <param name="x">    The x coordinate Date/Time. </param>
        ///
        /// <returns>   <c>true</c> if day; otherwise <c>false</c> </returns>

        protected bool IsDay(DateTime x)
        {
            return x.Hour == 0 && IsHour(x);
        }    

        /// <summary>   Query if 'x' is year. </summary>
        ///
        /// <param name="x">    The x coordinate Date/Time. </param>
        ///
        /// <returns>   <c>true</c> if year; otherwise <c>false</c> </returns>

        protected bool IsYear(DateTime x)
        {
            return x.DayOfYear == 1 && IsDay(x);
        }

        /// <summary>   Attempts to get separator indices from the given data. </summary>
        ///
        /// <param name="indices">                  . </param>
        /// <param name="maximumSeparatorCount">    Number of maximum separators. </param>
        /// <param name="separators">               [out]. </param>
        ///
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>

        public override bool TryGetSeparatorIndices(IEnumerable<double> indices, int maximumSeparatorCount, out IEnumerable<double> separators)
        {
            // First validate the interval between the indices
            // We expect always at least 2 indices to exist.
            var testdateTimes = indices.Take(2).Select(d => DateAxisCore.GetdateTime(d)).ToList();
            var distance = testdateTimes[1].Subtract(testdateTimes[0]);

            // Validate the distance between the separators.
            // This should be a sane value for this window.
            // I.e. Seconds displayed with intervals of hours, is not sane
            if (!Validate(distance))
            {
                // This date axis window does not validate the range between these 
                separators = Enumerable.Empty<double>();
                return false;
            }

            return base.TryGetSeparatorIndices(indices, maximumSeparatorCount, out separators);
        }

        /// <summary>   Validates the given seperator distance. </summary>
        ///
        /// <param name="seperatorDistance">    . </param>
        ///
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>

        protected virtual bool Validate(TimeSpan seperatorDistance)
        {
            return true;
        }

        /// <summary>   Determines whether a dateTime is a header. </summary>
        ///
        /// <param name="x">    The x coordinate Date/Time. </param>
        ///
        /// <returns>   <c>true</c> if header; otherwise <c>false</c> </returns>

        public abstract bool IsHeader(DateTime x);

        /// <summary>   Gets or sets a function to determine whether a dateTime is a separator. </summary>
        ///
        /// <param name="x">    The x coordinate Date/Time. </param>
        ///
        /// <returns>   <c>true</c> if separator; otherwise <c>false</c> </returns>

        public abstract bool IsSeparator(DateTime x);

        /// <summary>   Gets or sets a function to format the label for the axis. </summary>
        ///
        /// <param name="x">    The x coordinate Date/Time. </param>
        ///
        /// <returns>   The formatted axis label. </returns>

        public abstract string FormatAxisLabel(DateTime x);

    }
}