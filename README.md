# Live Charts Project

Support Charting using a custom chart control.

## Getting Started

Clone the project into its requisite relative path:
```
.\Libraries\VS\Visuals\Live
git clone git@bitbucket.org:davidhary/live.git
```

## Testing

The project includes a few unit test classes. Test applications are under the *Demos* solution folder. 

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* [Beto Rodriguez](https://github.com/Live-Charts/) 

## License

This project is licensed under the [MIT License](https://www.bitbucket.org/davidhary/vs.live.charts/src/master/LICENSE.md)

## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow](https://www.stackoveflow.com)

## Revision Changes

* [Live-Charts](https://github.com/Live-Charts/) 
