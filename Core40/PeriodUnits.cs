namespace LiveCharts.Helpers
{
    /// <summary>
    /// Defines the interval on which earch of the charts bars is based
    /// </summary>
    public enum PeriodUnits
    {
        /// <summary>   An enum constant representing the milliseconds option. </summary>
        Milliseconds,
        /// <summary>   An enum constant representing the seconds option. </summary>
        Seconds,
        /// <summary>   An enum constant representing the minutes option. </summary>
        Minutes,
        /// <summary>   An enum constant representing the hours option. </summary>
        Hours,
        /// <summary>   An enum constant representing the days option. </summary>
        Days
    }
}