﻿//The MIT License(MIT)

//Copyright(c) 2016 Alberto Rodriguez & LiveCharts Contributors

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

using System.Collections.Generic;
using System.Linq;

namespace LiveCharts
{
    /// <summary>   Form for viewing the axis. </summary>
    ///

    public abstract class AxisWindow : IAxisWindow
    {
        /// <summary>   Gets the minimum reserved space for separators. </summary>
        ///
        /// <value> The width of the minimum separator. </value>

        public abstract double MinimumSeparatorWidth { get; }

        /// <summary>   Determines whether a dateTime is a header. </summary>
        ///
        /// <param name="x">    The x coordinate. </param>
        ///
        /// <returns>   <c>true</c> if header; otherwise <c>false</c> </returns>

        public abstract bool IsHeader(double x);

        /// <summary>   Gets or sets a function to determine whether a dateTime is a separator. </summary>
        ///
        /// <param name="x">    The x coordinate. </param>
        ///
        /// <returns>   <c>true</c> if separator; otherwise <c>false</c> </returns>

        public abstract bool IsSeparator(double x);

        /// <summary>   Gets or sets a function to format the label for the axis. </summary>
        ///
        /// <param name="x">    The x coordinate. </param>
        ///
        /// <returns>   The formatted axis label. </returns>

        public abstract string FormatAxisLabel(double x);

        /// <summary>   Attempts to get separator indices from the given data. </summary>
        ///
        /// <param name="indices">                  The indices. </param>
        /// <param name="maximumSeparatorCount">    Number of maximum separators. </param>
        /// <param name="separators">               [out] The separators. </param>
        ///
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>

        public virtual bool TryGetSeparatorIndices(IEnumerable<double> indices, int maximumSeparatorCount, out IEnumerable<double> separators)
        {
            var separatorList = new List<double>();

            foreach (var index in indices)
            {
                if (separatorList.Count > maximumSeparatorCount)
                {
                    // We have exceeded the maxmimum separators.
                    separators = Enumerable.Empty<double>();
                    return false;
                }

                if (!IsSeparator(index)) continue;

                // Add this separator
                separatorList.Add(index);
            }

            // Return the separators
            separators = separatorList;
            return true;
        }
    }
}